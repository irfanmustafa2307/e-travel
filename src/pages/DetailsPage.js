import React, { Component } from 'react'
import {connect} from 'react-redux'

import Header from 'parts/Header'
import PageDetailTitle from 'parts/PageDetailTitle'
import PageDetailDescription from 'parts/PageDetailDescription'
import BookingForm from 'parts/BookingForm'
import Footer from 'parts/Footer'

import { checkoutBooking } from 'store/action/checkout'
import { fetchPage } from 'store/action/page'

class DetailsPage extends Component {

    componentDidMount() {
        window.title = "Details Page"
        window.scrollTo(0, 0)

        if(!this.props.page[this.props.match.params.id])
        this.props.fetchPage(`${process.env.REACT_APP_HOST}/api/v1/detail-page/${this.props.match.params.id}`, 
        this.props.match.params.id
        )
    }

    render() {
        const { page, match } = this.props;
        console.log(page)

        if(!page[match.params.id]) return null

        const breadcrumb = [
            { pageTitle: "Home", pageHref: ""},
            { pageTitle: "Detail Mobil", pageHref: ""}
        ]

        return (
            <div>
                <Header {...this.props}></Header>
                <PageDetailTitle breadcrumb={breadcrumb} data={page[match.params.id]}></PageDetailTitle>
                <div className="container">
                    <div className="row mb-5">
                        <div className="col-7 pr-5">
                            <PageDetailDescription data={page[match.params.id]}></PageDetailDescription>
                        </div>
                        <div className="col-5">
                             <BookingForm itemDetails={page[match.params.id]} startBooking={this.props.checkoutBooking}></BookingForm>
                        </div>
                    </div>
                </div>
                <Footer />
                
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    page: state.page,
})

export default connect(mapStateToProps, { checkoutBooking, fetchPage})(DetailsPage);