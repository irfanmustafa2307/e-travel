import React, { Component } from 'react'
import {connect} from 'react-redux'

import Header from 'parts/Header'
import Hero from 'parts/Hero'
import Categories from 'parts/Categories'
import Footer from 'parts/Footer'

import { fetchPage } from 'store/action/page'

class LandingPage extends Component {
    constructor(props){
        super(props);
        this.refCategories = React.createRef();
    }

    componentDidMount() {
        window.title = "Rental Mobil | Home"
        window.scrollTo(0, 0)

        if(!this.props.page.landingPage)
            this.props.fetchPage(`${process.env.REACT_APP_HOST}/api/v1/landing-page`, 
            "landingPage"
            )
    }

    render() {
        const { page } = this.props;

        console.log(page)

        if(!page.hasOwnProperty("landingPage")) return null;

        return (
            <>
            <Header {...this.props}></Header>
            <Hero  refCategories={this.refCategories} />
            <Categories refCategories={this.refCategories} data={page.landingPage.categories} />
            <Footer />
            
            </>
        )
    }
}

const mapStateToProps = (state) => ({
    page: state.page,
})

export default connect(mapStateToProps, {fetchPage})(LandingPage)