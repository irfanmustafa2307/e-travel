import React from 'react'
import { Fade } from "react-awesome-reveal";

import Button from 'elements/Button'

export default function Categories(props) {

    return props.data.map( (category, index1) => {
        return(  
        <section  className="container " ref={props.refCategories} key={`category-${index1}`}>
            <Fade>
            <h4 className="mb-3 font-weight-medium title-categories" >{category.name}
            </h4>
            <div className="container-grid">
                {
                    category.itemId.length === 0 ? <div className="row"> 
                        <div className="col-auto align-items-center">
                        There is no property at this category
                        </div>
                    </div> : category.itemId.map( (item, index2) => {
                        return <div className="item column-3 row-1" key={`category-${index1}-item-${index2}`}>
                            <div className="card">
                                {item.isPopular && <div className="tag">
                                    Popular{" "}<span className="font-weight-light">Choice</span></div>}
                                <figure className="img-wrapper" style={{ height: 180}}>
                                    <img src={`https://rental-mobil-server.herokuapp.com/${item.imageUrl[0].image}`} alt={item.name} className="img-cover"/>
                                </figure>
                                <div className="meta-wrapper">
                                    <Button type="link" href={`properties/${item._id}`} className="stretched-link d-block">
                                        <h5>{item.name}</h5>
                                    </Button>
                                    <span>{item.city}, {item.country}</span>
                                </div>
                            </div>
                               
                            </div>
                    })
                }
            </div>
            </Fade>
        </section>
        
        )
    })
    
}
