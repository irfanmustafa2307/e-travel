import React from 'react'

import Button from 'elements/Button'

export default function IconText() {
    return (
        <Button className="brand-text-icon" href="" type="link">
        rentalmobil.<span className="text-gray-700">id</span>

        </Button>
    )
}
