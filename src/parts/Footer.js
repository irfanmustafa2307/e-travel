import React from 'react'
import { Fade } from "react-awesome-reveal";

import Button from 'elements/Button'
import IconText from 'parts/IconText'

export default function Footer() {
    return (
        <Fade>
        <footer>
        <div className="container">
            <div className="row">
                <div className="col text-center copyrights">
               <p> Copyright 2021 • Irfan Eduwork • rentalmobil.id</p>
                </div>
            </div>
        </div>
        
    </footer>
    </Fade>
    )
}
