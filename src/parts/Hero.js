import React from 'react' //rfc
import { Fade } from "react-awesome-reveal";

import ImageHero from 'assets/images/img-hero5.png'
import Button from 'elements/Button'

export default function Hero(props) {
    
    function ShowCategory() {
        window.scrollTo({
            top: props.refCategories.current.offsetTop - 30,
            behavior: "smooth"
        })
    }

    return (
        <Fade>
        <section className="container pt-4" style={{ height: 420}}>
            <div className="row align-items-center">
                <div className="col-auto pr-5" style={{ width: 500 }}>
                <h1 className="font-weight-bold line-height-1 mb-3">
                    Mau Jalan Jalan ? <br />
                    Sewa Mobil Sekarang !
                </h1>
                <p  className="mb-4 font-weight-light text-gray-600 w-75" style={{ lineHeight: "170%" }}>
                Kami menyediakan mobil yang Anda butuhkan untuk menikmati perjalanan Anda bersama keluarga. 
                </p>
                <Button className="btn px-5" hasShadow isPrimary onClick={ShowCategory} >
                    Lihat Mobil
                </Button>
                </div>

                <div className="col-6 pl-5">
                    <div style={{ width: 520, height: 200}}>
                        <img src={ImageHero} alt="Image Hero" className="img-fluid position-absolute"
                        />
                    </div>
                </div>

            </div>
            
        </section>
        </Fade>
    )
}
