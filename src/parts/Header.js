import React from 'react'
import { Fade } from "react-awesome-reveal";

import Button from 'elements/Button'
import BrandIcon from "parts/IconText";

export default function Header(props) {

    const getNavLinkClass = (path) => {
        return props.location.pathname === path ? " active" : "";
      };

    if (props.isCentered)
    return (
      <Fade>
        <header className="spacing-sm">
          <div className="container">
            <nav className="navbar navbar-expand-lg navbar-light">
              <Button className="brand-text-icon mx-auto" href="" type="link">
                rentalmobil.<span className="text-gray-900">id</span>
              </Button>
            </nav>
          </div>
        </header>
        </Fade>
    );

    return (
      <Fade>
        <header className="spacing-sm">
        <div className="container">
          <nav className="navbar navbar-expand-lg navbar-light">
            <BrandIcon />
            <div className="collapse navbar-collapse">
              <ul className="navbar-nav ml-auto">
                <li className={`nav-item${getNavLinkClass("/")}`}>
                  <Button className="nav-link" type="link" href="/">
                    Home
                  </Button>
                </li>
                <li className={`nav-item${getNavLinkClass("/paket-travel")}`}>
                  <Button className="nav-link" type="link" href="/#">
                    Daftar Mobil
                  </Button>
                </li>
                <li className={`nav-item${getNavLinkClass("/testimonial")}`}>
                  <Button className="nav-link" type="link" href="/#">
                    Profil
                  </Button>
                </li>
                <li className={`nav-item${getNavLinkClass("/informasi")}`}>
                  <Button className="nav-link" type="link" href="/#">
                    Informasi
                  </Button>
                </li>
              </ul>
            </div>
          </nav>
        </div>
      </header>
      </Fade>
    )
}
