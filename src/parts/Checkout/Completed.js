import React from "react";
import { Fade } from "react-awesome-reveal";
import CompletedIllustration from "assets/images/success.png";

export default function Completed() {
  return (
    <Fade>
      <div className="container" style={{ marginBottom: 30 }}>
        <div className="row justify-content-center text-center">
          <div className="col-4">
            <img
              src={CompletedIllustration}
              className="img-fluid"
              alt="completed checkout apartment"
            />
            <p className="text-gray-500 pt-3">
            Kami akan menginformasikan melalui email nanti setelah transaksi diterima
            </p>
          </div>
        </div>
      </div>
    </Fade>
  );
}
